package com.xiaobai.yyds.log.enums;

/**
 * 操作状态
 * 
 * @author xiaobai
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
