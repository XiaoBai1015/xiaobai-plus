package com.xiaobai.yyds.core.constant;

/**
 * 服务名称
 * 
 * @author xiaobai
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "xiaobai-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "xiaobai-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "xiaobai-file";
}
