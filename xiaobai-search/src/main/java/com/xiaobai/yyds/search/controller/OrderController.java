package com.xiaobai.yyds.search.controller;


import com.xiaobai.yyds.search.service.OrderService;
import com.xiaobai.yyds.core.constant.HttpStatus;
import com.xiaobai.yyds.core.web.domain.AjaxResult;
import com.xiaobai.yyds.search.domain.Order;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author XIAOBAI
 */
@Slf4j
@Api(value = "ES订单信息管理", tags = "ES订单信息管理")
@RequestMapping("/order")
@RestController
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 批量创建
     */
    @PostMapping("saveBatch")
    @ApiOperation(value = "批量创建订单")
    public AjaxResult saveBatch(@RequestBody List<Order> orders) {
        if (CollectionUtils.isEmpty(orders)) {
            return  AjaxResult.error(HttpStatus.ERROR,"文档不能为空");
        }
        orderService.saveAll(orders);
        return AjaxResult.success("保存成功");
    }

    /**
     * 根据id删除
     */
    @ApiOperation(value = "根据id删除订单")
    @GetMapping("deleteById")
    public AjaxResult deleteById(@RequestParam Integer id) {
        try{
            orderService.deleteById(id);
            return AjaxResult.success("删除成功");
        }catch (Exception e){
            log.error("[ES]订单删除失败"+e.getMessage());
            return AjaxResult.error(HttpStatus.ERROR,"删除失败");
        }
    }

    /**
     * 根据id更新
     */
    @ApiOperation(value = "根据id更新订单")
    @PostMapping("updateById")
    public AjaxResult updateById(@RequestBody Order order) {
        try{
            orderService.updateById(order);
            return AjaxResult.success("更新成功");
        }catch (Exception e){
            log.error("[ES]订单更新失败"+e.getMessage());
            return AjaxResult.error(HttpStatus.ERROR,"更新失败");
        }
    }

    /**
     * 根据id搜索
     */
    @GetMapping("findById")
    @ApiOperation(value = "根据id搜索订单")
    public AjaxResult findById(@RequestParam Integer id) {
        Order order = orderService.findById(id);
        return order!=null ?  AjaxResult.success(order): AjaxResult.error(HttpStatus.SUCCESS,"暂无此信息");
    }

    /**
     * 内容模糊查询
     */
    @GetMapping("selectByOrderDesc")
    @ApiOperation(value = "文档内容模糊查询(不包含通过id查询)")
    public AjaxResult selectByOrderDesc(@RequestParam String info) {
        return orderService.selectByOrderDesc(info);
    }


    /**
     * 排序，分页查询
     * @param pageNum 第几页
     * @param pageSize 每页显示条数
     * @param sort 0升序，1降序
     * @param sortProperties  排序属性 ，字段名
     * @return
     */
    @GetMapping("selectPageOrder")
    @ApiOperation(value = "排序，分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "第几页（0开始）", required = true ,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "sort", value = "0升序，1降序", required = true,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "sortProperties", value = "排序属性 ，字段名(除orderDesc)", required = true,dataType = "String",dataTypeClass = String.class, paramType = "path")})
    public AjaxResult selectPageOrder(Integer pageNum,Integer pageSize, Integer sort, String sortProperties) {
        return orderService.selectPageOrder(pageNum, pageSize, sort, sortProperties);
    }


    /**
     * 排序，分页查询，高亮，根据订单描述信息查询
     * @param orderDesc 订单描述
     * @param pageNum 第几页
     * @param pageSize 每页显示条数
     * @param sort 0升序，1降序
     * @param sortProperties  排序属性 ，字段名
     * @return
     */
    @GetMapping("getSearchOrder")
    @ApiOperation(value = "排序,分页查询,高亮,根据订单描述信息查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderDesc", value = "订单描述", required = true,dataType = "String",dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "pageNum", value = "第几页（0开始）", required = true ,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "sort", value = "0升序，1降序", required = true,dataType = "Integer",dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "sortProperties", value = "排序属性 ，字段名(除orderDesc)", required = true,dataType = "String",dataTypeClass = String.class, paramType = "path")})
    public AjaxResult getSearchOrder(String orderDesc, Integer sort, Integer pageNum, Integer pageSize, String sortProperties) {
        return orderService.getSearchOrder(orderDesc, sort, pageNum, pageSize, sortProperties);
    }
}
