package com.xiaobai.yyds.search.mapper;


import com.xiaobai.yyds.search.domain.Order;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author XIAOBAI
 */
public interface OrderRepository extends ElasticsearchRepository<Order, Integer> {
}
