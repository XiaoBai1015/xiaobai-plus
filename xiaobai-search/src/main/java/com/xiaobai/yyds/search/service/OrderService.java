package com.xiaobai.yyds.search.service;


import com.xiaobai.yyds.core.web.domain.AjaxResult;
import com.xiaobai.yyds.search.domain.Order;

import java.util.List;

/**
 * @author XIAOBAI
 */
public interface OrderService {
    /***
     * 查询所有
     * @param orders
     */
    void saveAll(List<Order> orders);

    /**
     * 通过主键查询
     * @param id
     * @return
     */
    Order findById(Integer id);

    /**
     * 通过主键删除
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 修改信息
     * @param order
     */
    void updateById(Order order);

    /**
     * 内容模糊查询
     * @param info
     * @return
     */
    AjaxResult selectByOrderDesc(String info);

    /**
     * 排序，分页查询
     * @param pageNum 第几页
     * @param size 每页显示条数
     * @param sort 0升序，1降序
     * @param sortProperties  排序属性 ，字段名
     * @return
     */
    AjaxResult selectPageOrder(Integer pageNum,Integer size, Integer sort, String sortProperties);

    /**
     * 排序，分页查询，高亮，根据订单描述信息查询
     * @param orderDesc 订单描述
     * @param pageNum 第几页
     * @param pageSize 每页显示条数
     * @param sort 0升序，1降序
     * @param sortProperties  排序属性 ，字段名
     * @return
     */
    AjaxResult getSearchOrder(String orderDesc, Integer sort, Integer pageNum, Integer pageSize, String sortProperties);
}
