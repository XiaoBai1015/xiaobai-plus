package com.xiaobai.yyds.search.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/**
 * @author XIAOBAI
 *
 * @Document 作用在类，标记实体类为文档对象，一般有四个属性
 * indexName：对应索引库名称
 * type：对应在索引库中的类型（ElasticSearch7.x已经不推荐使用type了，并且将在8.0彻底移除）
 * shards：分片数量，默认1
 * replicas：副本数量，默认1
 * createIndex：如果将此参数设置为true（这是默认值），则Spring Data Elasticsearch将在启动应用程序启动时引导存储库支持期间检查由@Document注释定义的索引是否存在。如果不存在，那么将创建索引，并且将从实体的注释派生的映射。（当然我使用后发现，创建索引是延迟加载的，只有你使用之前才会创建。）
 * @Id 作用在成员变量，标记一个字段作为id主键
 * @Field 作用在成员变量，标记为文档的字段，并指定字段映射属性：
 * type：字段类型，取值是枚举：FieldType
 * index：是否索引，布尔类型，默认是true
 * store：是否存储，布尔类型，默认是false
 * analyzer：分词器名称：ik_max_word
 */
@Data
@Document(indexName = "order", shards = 1, replicas = 1)
public class Order implements Serializable {
    @Id
    private String id;

    @Field(type = FieldType.Keyword)
    private Long orderNo;

    @Field(type = FieldType.Integer)
    private Integer orderType;

    @Field(type = FieldType.Long)
    private Long orderAmount;

    @Field(type = FieldType.Text,  analyzer = "ik_max_word")
    private String orderDesc;

    @Field(type = FieldType.Keyword)
    private String username;

    @Field(type = FieldType.Keyword)
    private String userPhone;

}
