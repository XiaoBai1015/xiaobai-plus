package com.xiaobai.yyds.auth.config;


import com.xiaobai.yyds.swagger.BaseSwaggerConfig;
import com.xiaobai.yyds.swagger.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * Swagger API文档相关配置
 *
 * @author xiaobai
 * @date 2021/4/26
 */
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("com.xiaobai.yyds.auth.controller")
                .title("小白认证中心")
                .description("小白认证中心相关接口文档")
                .contactName("小白")
                .version("1.0")
                .enableSecurity(false)
                .build();
    }
}
