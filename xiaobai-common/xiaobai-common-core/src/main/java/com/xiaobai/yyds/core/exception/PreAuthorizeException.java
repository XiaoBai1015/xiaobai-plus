package com.xiaobai.yyds.core.exception;

/**
 * 权限异常
 * 
 * @author xiaobai
 */
public class PreAuthorizeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException()
    {
    }
}
